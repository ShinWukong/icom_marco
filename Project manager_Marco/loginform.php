<?php
 require_once('inc/config.php');
 ?>
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<title> Task Manager </title>
		<link rel="stylesheet" type="text/css" href="css/main.css" />
<link rel="stylesheet" type="text/css" href="css/app.css" />
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
</head>
<body>
  <header>
    <div class="header-title">Project Management</div>
  </header>

    <div class="form">
      <ul class="tab-group">
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>

      <div class="tab-content">
          <h1>Welcome Back!</h1>

	          <form action="/" method="post">

	          <div class="field-form">
	            <label>
                <span class="form-label">name</span>
	            </label>
              <span><textarea class="form-textarea"></textarea></span>
	          </div>

	          <div class="field-form">
	            <label>
	              <span class="form-label">Password</span>
	            </label>
              <input type="password">
	          </div>

	         <a href="addform.php"><button class="button button-block"/>Log In</button></a>

	          </form>

	        </div>

	      </div><!-- tab-content -->

	</div>
      </body>
</html>
