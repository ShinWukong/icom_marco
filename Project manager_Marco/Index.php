<?php
 require_once('inc/config.php');
 ?>

<!doctype html>
  <html class="no-js" lang="">
    <head>
    	<meta charset="UTF-8">
    	<link rel="stylesheet" href="css/main.css">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    	<title> Projet Manager </title>>
    </head>
    <body>
      <div class="container">
          <header>
            <div class="header-title">Project Management</div>
            <button type="button" class="projet-Logout"></button>Bye</span>
          </header>
        <ul class="projet-tab">

          <li class="projet-head">
            <span class="projet-january">January</span>
            <span class="projet-february">February</span>
            <span class="projet-march">March</span>
            <span class="projet-april">April</span>
            <span class="projet-may">May</span>
            <span class="projet-june">June</span>
            <span class="projet-july">July</span>
            <span class="projet-august">August</span>
            <span class="projet-september">September</span>
            <span class="projet-october">October</span>
            <span class="projet-november">November</span>
            <span class="projet-december">December</span>
          </li>

        <?php foreach ($data as $row) :?>
          <li class="projet-table">
             <span class="projet-item-id"><?php echo $row['id']?></span>
              <span class="projet-item-description"><?php echo $row['name']?></span>
              <span class="projet-item-date"><?php echo $row['start']?><button type="button" class="projet-item-start"></button></span>
              <span class="projet-item-duedate"><?php echo $row['end']?><button type="button" class="projet-item-end"></button></span>
              <span class="projet-item-delete"><a href="delete.php?id=<?php echo $row['id']?>" class="project-erase">✘</a></span>
          </li>
        <?php endforeach;?>

        </ul>

          <a href="addform.php"><button type="button" class="projet-add">add</button></a>

    </div>

    </body>


  </html>
